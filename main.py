import time
from core import RSA
from test import RSA_hastad, DH_MITM
import argparse
import sys
import re
import os
import logging
import datetime

def logg(time, info, arguments = None):
	if arguments:
		logging.info("[ " + str(time) + " ] " +  str(info) + " ( " + arguments + " ) ")
	else:
		logging.info("[ " + str(time) + " ] " + str(info))

timepoint = time.time()

def cp():
	timepoint = time.time()

def last():
	return time.time() - timepoint

def save_keypair(openkey, privatekey):
	with open("o.key", 'wb') as openkeyfile, open("p.key", 'wb') as privatekeyfile:
		
		openkeyfile.write(openkey[0].to_bytes(openkey[0].bit_length() // 8 + 1,'big') + b"-:-" + openkey[1].to_bytes(openkey[1].bit_length() // 8 + 1,'big'))
		privatekeyfile.write(privatekey[0].to_bytes(privatekey[0].bit_length() // 8 + 1,'big') + b"-:-" + privatekey[1].to_bytes(privatekey[1].bit_length() // 8 + 1,'big'))
		
		openkeyfile.close()
		privatekeyfile.close()

def openkey(keyfile):

	data = open(keyfile, 'rb').read()
	key = re.split(rb'-:-', data)

	return (int.from_bytes(key[0], 'big'), int.from_bytes(key[1], 'big'))

def enc(filename, openkey):
	open_text = open(filename, 'rb').read()
	cp()
	cyphertext = rsa_crypt.encrypt(open_text, openkey)
	logg(last(), "Зашифрован файл", "cyphertext")
	print(cyphertext)
	with open(filename + ".gpg", "wb") as file:
		file.write(cyphertext)
		file.close()

def dec(filename, privatekey):
	cyphertext = open(filename, 'rb').read()
	cp()
	open_text = rsa_crypt.decrypt(cyphertext, privatekey)
	logg(last(), "Расшифрован файл", "cyphertext")
	print(open_text)
	with open(filename[0:filename.find(".gpg")], "wb") as file:
		file.write(open_text)
		file.close()

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('-e', '--encrypt', action='store_const', const=True, default=False)
	parser.add_argument('-d', '--decrypt', action='store_const', const=True, default=False)
	parser.add_argument('-g', '--generate', action='store_const', const=True, default=False)
	parser.add_argument('-i', '--input', default=None, type=str)
	parser.add_argument('-k', '--key', default=None, type=str)
	parser.add_argument('-t', '--test', action='store_const', const=True, default=False)
	parser.add_argument('-exp', '--exponent', default=3, type=int)
	parser.add_argument('-bit', '--bits', default=1024, type=int)
	key = parser.parse_args()

	date = str(datetime.date.today())
	filename = r'\log'+date+'.log'

	logging.basicConfig(format = u'%(filename)s[%(levelname)s]:[%(asctime)s][%(lineno)s] --> %(message)s', level = 0, filename = os.getcwd() + filename)

	rsa_crypt = RSA()

	# Генерация ключей
	if key.generate:
		cp()
		openkey, privatekey = rsa_crypt.generate_key_pair()
		logg(last(), "Сгенерирована пара ключей", "open key, private key")

		save_keypair(openkey, privatekey)
		print(openkey, privatekey)
		
		if key.input:
			enc(key.input, openkey)
		
		sys.exit(1)

	# Шифрование
	if key.encrypt and key.input and key.key:
		k = openkey(key.key)
		enc(key.input, k)
		sys.exit(1)

	# Расшифровка
	if key.decrypt and key.input and key.key:
		k = openkey(key.key)
		dec(key.input, k)
		sys.exit(1)



	# Демонстрация атак
	if key.test:

		logg(time.time(), "Атака Хастада на малую экспоненту", "message, exponent")
		messg = "Message to be encrypted."
		exponent = key.exponent
		logg(time.time(), messg + " "+ str(exponent))

		rsa_crypt= RSA(bits=key.bits, exp=exponent)
		captured = []
		
		logg(time.time(), "Генерация пар ключей")
		for i in range(0,exponent):
			cp()
			openk, closedk = rsa_crypt.generate_key_pair()
			while not closedk[0]:
				openk, closedk = rsa_crypt.generate_key_pair()
			logg(last(), "Пара ключей сгенерирована", "open key, private key")
			print(openk, closedk)

			cp()
			ciphertext = rsa_crypt.encrypt(messg, openk)
			logg(last(), "Зашифровано сообщение", "cyphertext")
			logg(time.time(), ciphertext)

			captured.append((ciphertext, openk))

		cp()
		rsa_hastad = RSA_hastad(captured)
		logg(last(), "Завершена атака Хастада", "result")
		logg(time.time(), rsa_hastad.decrypt())

		logg(time.time(), "Демонстрация MITM на протокол Диффи-Хеллмана")
		mitm = DH_MITM()
		mitm.run()