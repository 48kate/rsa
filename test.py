from random import randrange as rr
from functools import reduce
from core import invmod, RSA
from Crypto.Util import number

import logging
import utils


class DH_MITM(object):
	"""docstring for ClassName"""
	def __init__(self, bits=1024):
		self.bits = bits

	def run(self):
		rsa_crypt = RSA(bits=self.bits)
		key, _ = rsa_crypt.generate_key_pair()

		# Alice
		a = number.getPrime(self.bits >> 1)
		message_a = pow(key[0], a, key[1])
		logging.info("Алиса генерирует a: " + str(a) + " message: " + str(message_a))

		# Bob
		b = number.getPrime(self.bits >> 1)
		message_b = pow(key[0], b, key[1])
		logging.info("Боб генерирует b: " + str(b) + " message: " + str(message_b))

		# Hacker
		ha = number.getPrime(self.bits >> 1)
		hb = number.getPrime(self.bits >> 1)
		message_ha = pow(key[0], ha, key[1])
		message_hb = pow(key[0], hb, key[1])
		logging.info("Хакер генерирует ha: " + str(ha) + "  hb: " + str(hb) + " message ha: " + str(message_ha) +  " message hb: " + str(message_hb))
		key_ha = pow(message_a, hb, key[1])
		key_hb = pow(message_b, ha, key[1])
		logging.info("Хакер считает ключи ha: " + str(key_ha) + " hb: " + str(key_hb))

		# Alice
		key_a = pow(message_hb, a, key[1])
		# Bob
		key_b = pow(message_ha, b, key[1])
		logging.info("Алиса считает ключ: " + str(key_a) + " Боб считает ключ: " + str(key_b))


class RSA_hastad(object):

	ciphertexts = []
	openkeys = []
	exp = 3

	def __init__(self, captured):
		for frame in captured:
			self.ciphertexts.append(frame[0])
			self.openkeys.append(frame[1])
	
	def is_possible(self):
		exp = self.openkeys[0][0]
		for pair in self.openkeys:
			if exp != pair[0]:
				return False
		self.exp = exp

		if exp <= len(self.ciphertexts):
			return True
		else:
			return False

	 

	def decrypt(self):
		if not self.is_possible():
			return "Attack can't proceed"
		else:

			mods = list(self.openkeys[i][1] for i in range(0, len(self.openkeys)))
			texts = list(int.from_bytes(self.ciphertexts[i], 'big', signed=False) for i in range(0, len(self.ciphertexts)))
			
			root = utils.CRT(mods, texts)
			
			nroot = utils.nth_root(root, len(texts))
			print(int(nroot).to_bytes(256, 'big'))
			if nroot ** 3 == root:
				return root
			else:
				return "Root not found"
