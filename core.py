#from math import *
from random import randrange as rr
from random import randrange, getrandbits
from Crypto.Util import number
import os
import re

def binpow(a, e, mod):
	if e == 0:
		return 1
	elif e % 2 == 1:
		return a * binpow(a, e - 1, mod) % mod
	else:
		return pow(binpow(a, e / 2, mod), 2) % mod

def gcd(a, b):
	if a == 0:
		return b, 0, 1
	d, x, y = gcd(b % a, a)
	return d, y - (b // a) * x, x

def invmod(a, m):
	g, x, y = gcd(a, m)
	if g != 1:
		return None
	else:
		return (x % m + m) % m

def ceil_div(num: int, div: int) -> int:
	q, m = divmod(num, div)
	if m: q += 1
	return q

def byte_size(num: int) -> int:
	if num == 0:
		return 1
	return ceil_div(num.bit_length(), 8)

def pad(message: bytes, target_length: int) -> bytes:
    # Pads the message for encryption, returning the padded message. return 00 02 RANDOM_DATA 00 MESSAGE

    max_msglength = target_length - 11
    msglength = len(message)

    if msglength > max_msglength:
        raise OverflowError('%i bytes needed for message, but there is only'
                            ' space for %i' % (msglength, max_msglength))

    # Get random padding
    padding = b''
    padding_length = target_length - msglength - 3

    # We remove 0-bytes, so we'll end up with less padding than we've asked for,
    # so keep adding data until we're at the correct length.
    while len(padding) < padding_length:
        needed_bytes = padding_length - len(padding)

        # Always read at least 8 bytes more than we need, and trim off the rest
        # after removing the 0-bytes. This increases the chance of getting
        # enough bytes, especially when needed_bytes is small
        new_padding = os.urandom(needed_bytes + 5)
        new_padding = new_padding.replace(b'\x00', b'')
        padding = padding + new_padding[:needed_bytes]

    assert len(padding) == padding_length

    return b''.join([b'\x00\x02',
                     padding,
                     b'\x00',
                     message])

class RSA(object):

	def __init__(self, bits=1024, exp=65537):
		self.bits = bits
		self.exp = exp


	def generate_key_pair(self):
		assert self.bits >= 512

		p = number.getPrime(self.bits >> 1)
		q = number.getPrime(self.bits >> 1)

		phi = (p - 1) * (q - 1)
		n = p * q
		e = self.exp
		d = invmod(e, phi)

		return ((e,n),(d,n))

	def crypt(self, block, key, mod):

		if not type(block) is bytes:
			block = block.encode('utf-8')
		blockint = int.from_bytes(block, 'big', signed=False)

		if blockint > mod:
			raise OverflowError('msg too long')

		return int(pow(blockint, key, mod))

	def encrypt(self, message, key):
		power = key[0]
		mod = key[1]

		bs = byte_size(mod) 
		if not type(message) is bytes:
			message = message.encode('utf-8')
		step = bs - 7
		data_array = (pad(message[i:i+step], bs) for i in range(0, len(message), step))

		return b''.join(self.crypt(block, power, mod).to_bytes(bs, 'big') for block in data_array)


	def decrypt(self, message, key):
		power = key[0]
		mod = key[1]
		
		bs = byte_size(mod)
		data_array = (message[i:i+bs] for i in range(0, len(message), bs))

		return b''.join(re.split(br"\x00",self.crypt(block, power, mod).to_bytes(bs, 'big'))[2] for block in data_array)


